<div align="center">

## Data Login User

- [x] https://dummyjson.com/users

## Technology

- [x] Flutter

## Dependencies in this project

- [x] BLOC
- [x] equatable
- [x] freezed
- [x] dartz

<a href="">
  <img src="assets/screenshoots/Screenshot_1693943915.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943919.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943923.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943926.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943931.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943935.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943955.png" alt="Logo" width="250" >
  <img src="assets/screenshoots/Screenshot_1693943960.png" alt="Logo" width="250" >
</a>

</div>
