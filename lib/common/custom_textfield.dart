// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:flutter_online_dummy_json/common/theme.dart';

class CustomTextField extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;
  final IconData icon;
  final bool obsecure;
  const CustomTextField({
    Key? key,
    required this.hintText,
    required this.controller,
    this.icon = Icons.person,
    required this.obsecure,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: controller,
      obscureText: obsecure,
      decoration: InputDecoration(
        prefixIcon: Icon(
          icon,
          color: dOrangeColor,
        ),
        hintText: hintText,
        hintStyle: TextStyle(
          color: dOrangeColor.withOpacity(.75),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: dOrangeColor, width: 2.0),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: dOrangeColor, width: 2.0),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: dOrangeColor, width: 2.0),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Enter your $hintText';
        }
        return null;
      },
    );
  }
}
