import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var baseUrl = 'https://dummyjson.com';

Color dOrangeColor = Color(0xFFE94B28);
Color dDarkPurpleColor = Color(0xFF5F3F93);
Color dPurpleColor = Color(0xFF9B88BE);

TextStyle dFontType = GoogleFonts.poppins();

FontWeight light = FontWeight.w300;
FontWeight regular = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBold = FontWeight.w800;
FontWeight black = FontWeight.w900;
