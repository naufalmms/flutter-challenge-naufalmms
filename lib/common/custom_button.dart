// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_online_dummy_json/common/theme.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final Function() onPressed;
  final EdgeInsets margin;
  final Color colorButton;
  final Color colorText;

  const CustomButton({
    Key? key,
    required this.title,
    required this.onPressed,
    required this.margin,
    required this.colorButton,
    required this.colorText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      margin: margin,
      child: TextButton(
          onPressed: onPressed,
          style: ButtonStyle(
            backgroundColor: MaterialStatePropertyAll(colorButton),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                side: BorderSide(
                  color: colorButton,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          ),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 24,
              fontWeight: medium,
              color: colorText,
            ),
          )),
    );
  }
}
