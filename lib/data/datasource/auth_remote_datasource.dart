import 'package:dartz/dartz.dart';
import 'package:flutter_online_dummy_json/data/model/request/login_request_model.dart';
import 'package:flutter_online_dummy_json/data/model/request/register_request_model.dart';
import 'package:flutter_online_dummy_json/data/model/response/login_response_model.dart';
import 'package:flutter_online_dummy_json/data/model/response/register_response_model.dart';
import 'package:http/http.dart' as http;

class AuthRemoteDatasource {
  Future<Either<String, LoginResponseModel>> login(
      LoginRequestModel model) async {
    final response = await http.post(
      Uri.parse('https://dummyjson.com/auth/login'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: model.toJson(),
    );

    if (response.statusCode == 200) {
      return Right(LoginResponseModel.fromRawJson(response.body));
    } else {
      return const Left('server error');
    }
  }

  Future<Either<String, RegisterResponseModel>> register(
      RegisterRequestModel model) async {
    final response = await http.post(
      Uri.parse('https://dummyjson.com/users/add'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: model.toJson(),
    );
    if (response.statusCode == 200) {
      return Right(RegisterResponseModel.fromRawJson(response.body));
    } else {
      return const Left('Server Error');
    }
  }
}
