part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class DoAuthEvent extends AuthEvent {
  final LoginRequestModel model;
  DoAuthEvent({
    required this.model,
  });
}
