// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthLoaded extends AuthState {
  final LoginResponseModel model;

  AuthLoaded({
    required this.model,
  });
}

class AuthError extends AuthState {}
