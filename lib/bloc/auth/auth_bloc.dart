import 'package:bloc/bloc.dart';
import 'package:flutter_online_dummy_json/data/datasource/auth_remote_datasource.dart';
import 'package:flutter_online_dummy_json/data/model/request/login_request_model.dart';
import 'package:flutter_online_dummy_json/data/model/response/login_response_model.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRemoteDatasource datasource;
  AuthBloc(
    this.datasource,
  ) : super(AuthInitial()) {
    on<DoAuthEvent>((event, emit) async {
      emit(AuthLoading());
      final result = await datasource.login(event.model);
      result.fold(
        (l) => emit(AuthError()),
        (r) => emit(AuthLoaded(model: r)),
      );
    });
  }
}
