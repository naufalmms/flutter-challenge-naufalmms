// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:flutter_online_dummy_json/data/datasource/product_remote_datasource.dart';
import 'package:flutter_online_dummy_json/data/model/response/list_product_response_model.dart';

part 'get_product_bloc.freezed.dart';
part 'get_product_event.dart';
part 'get_product_state.dart';

class GetProductBloc extends Bloc<GetProductEvent, GetProductState> {
  final ProductRemoteDataSource dataSource;
  GetProductBloc(
    this.dataSource,
  ) : super(const _Initial()) {
    on<GetProductEvent>((event, emit) async {
      emit(const _Loading());
      final result = await dataSource.getAllProduct();
      result.fold(
        (l) => emit(const _Error()),
        (r) => emit(_Loaded(r)),
      );
    });
  }
}
