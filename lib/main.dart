import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_online_dummy_json/bloc/auth/auth_bloc.dart';
import 'package:flutter_online_dummy_json/bloc/get_product/get_product_bloc.dart';
import 'package:flutter_online_dummy_json/bloc/register/register_bloc.dart';
import 'package:flutter_online_dummy_json/data/datasource/auth_remote_datasource.dart';
import 'package:flutter_online_dummy_json/data/datasource/product_remote_datasource.dart';
import 'package:flutter_online_dummy_json/presentation/auth/login_page.dart';
import 'package:flutter_online_dummy_json/presentation/home/main_page.dart';
import 'package:flutter_online_dummy_json/presentation/welcome/splash_page.dart';

import 'bloc/cubit/page_cubit.dart';
import 'presentation/auth/signup_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => PageCubit(),
          ),
          BlocProvider(
            create: (context) => AuthBloc(AuthRemoteDatasource()),
          ),
          BlocProvider(
            create: (context) => RegisterBloc(AuthRemoteDatasource()),
          ),
          BlocProvider(
            create: (context) => GetProductBloc(ProductRemoteDataSource()),
          ),
        ],
        child: MaterialApp(
          title: 'Flutter dummyJSON',
          debugShowCheckedModeBanner: false,
          routes: {
            '/': (context) => const SplashScreen(),
            '/login-page': (context) => LoginPage(),
            '/sign-up': (context) => SignupPage(),
            '/main': (context) => MainPage(),
          },
        ));
  }
}
