import 'package:flutter/material.dart';
import 'package:flutter_online_dummy_json/common/custom_button.dart';
import 'package:flutter_online_dummy_json/common/theme.dart';

class SettingPage extends StatelessWidget {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: CustomButton(
          title: "Logout",
          onPressed: () {},
          margin: const EdgeInsets.all(0),
          colorButton: dDarkPurpleColor,
          colorText: Colors.white,
        ),
      ),
    );
  }
}
