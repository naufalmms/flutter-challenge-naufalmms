// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_online_dummy_json/bloc/auth/auth_bloc.dart';
import 'package:flutter_online_dummy_json/common/custom_button.dart';
import 'package:flutter_online_dummy_json/common/custom_textfield.dart';
import 'package:flutter_online_dummy_json/common/theme.dart';
import 'package:flutter_online_dummy_json/data/model/request/login_request_model.dart';

import '../../common/custom_dialog.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final _signInFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget inputSection() {
      return Form(
        key: _signInFormKey,
        child: Column(
          children: [
            CustomTextField(
              hintText: "Login",
              controller: _emailController,
              icon: Icons.person,
              obsecure: false,
            ),
            const SizedBox(
              height: 20.0,
            ),
            CustomTextField(
              hintText: "Password",
              controller: _passwordController,
              icon: Icons.lock,
              obsecure: true,
            ),
          ],
        ),
      );
    }

    Widget signUpButton() {
      return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, "/sign-up");
        },
        child: Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(top: 50),
          child: Text(
            'Don\'t have an account? Sign Up',
            style: dFontType.copyWith(
              color: dOrangeColor,
              fontSize: 16,
              fontWeight: light,
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Positioned(
                  child: Image.asset(
                    "assets/header-login.png",
                    width: 150,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: 50,
                  left: 175,
                  child: Image.asset(
                    "assets/logo.png",
                    width: 100,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 75),
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 50.0,
                      ),
                      const SizedBox(
                        height: 100.0,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "LOGIN",
                            style: dFontType.copyWith(
                              color: Colors.black,
                              fontWeight: bold,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            "Please Sign in to continue",
                            style: dFontType.copyWith(
                              color: Colors.black,
                              fontSize: 15,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      inputSection(),
                      const SizedBox(
                        height: 20.0,
                      ),
                      BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, state) {
                          if (state is AuthLoaded) {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return CustomDialog(
                                  kata_1:
                                      "Selamat Datang ${state.model.firstName}",
                                  kata_2: "Data Login Benar",
                                  onPressed: () {
                                    Navigator.pushNamedAndRemoveUntil(
                                        context, '/main', (route) => false);
                                  },
                                );
                              },
                            );
                          } else if (state is AuthError) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                backgroundColor: Colors.red,
                                content: Text("Data Salah"),
                              ),
                            );
                          }
                        },
                        builder: (context, state) {
                          if (state is AuthLoading) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return CustomButton(
                            title: "Login",
                            onPressed: () {
                              if (_signInFormKey.currentState!.validate()) {
                                final model = LoginRequestModel(
                                  username: _emailController.text,
                                  password: _passwordController.text,
                                );
                                context
                                    .read<AuthBloc>()
                                    .add(DoAuthEvent(model: model));
                              } else {
                                showDialog(
                                  context: context,
                                  builder: (context) {
                                    return CustomDialog(
                                      kata_1: "Lengkapi Data",
                                      kata_2:
                                          "User ID dan atau Password anda belum diisi.",
                                      onPressed: () {
                                        Navigator.pop(context, 'OK');
                                      },
                                    );
                                  },
                                );
                              }
                            },
                            margin: const EdgeInsets.only(top: 20, left: 150),
                            colorButton: dDarkPurpleColor,
                            colorText: Colors.white,
                          );
                        },
                      ),
                      signUpButton(),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
