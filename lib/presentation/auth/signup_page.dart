// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_online_dummy_json/bloc/register/register_bloc.dart';
import 'package:flutter_online_dummy_json/common/custom_button.dart';
import 'package:flutter_online_dummy_json/common/custom_textfield.dart';
import 'package:flutter_online_dummy_json/common/theme.dart';
import 'package:flutter_online_dummy_json/data/model/request/register_request_model.dart';
import 'package:flutter_online_dummy_json/presentation/auth/login_page.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final TextEditingController firstNameController =
      TextEditingController(text: '');
  final TextEditingController lastNameController =
      TextEditingController(text: '');
  final TextEditingController usernameController =
      TextEditingController(text: '');
  final TextEditingController passwordController =
      TextEditingController(text: '');

  final _signUpFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget inputSection() {
      Widget firstNameInput() {
        return CustomTextField(
          hintText: "First Name",
          controller: firstNameController,
          obsecure: false,
        );
      }

      Widget lastNameInput() {
        return CustomTextField(
          hintText: "Last Name",
          controller: lastNameController,
          obsecure: false,
        );
      }

      Widget usernameInput() {
        return CustomTextField(
          hintText: "Username",
          controller: usernameController,
          obsecure: false,
        );
      }

      Widget passwordInput() {
        return CustomTextField(
          hintText: "Password",
          controller: passwordController,
          obsecure: true,
          icon: Icons.lock,
        );
      }

      Widget submitButton() {
        return BlocConsumer<RegisterBloc, RegisterState>(
          listener: (context, state) {
            state.maybeWhen(
              orElse: () {},
              error: () {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Register Error'),
                    backgroundColor: Colors.red,
                  ),
                );
              },
              loaded: (model) async {
                // Navigator.pushNamedAndRemoveUntil(
                //     context, '/login-page', (route) => false);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return const LoginPage();
                    },
                  ),
                );
              },
            );
          },
          builder: (context, state) {
            return state.maybeWhen(
              orElse: () {
                return CustomButton(
                  title: "Submit",
                  onPressed: () {
                    if (_signUpFormKey.currentState!.validate()) {
                      final requestModel = RegisterRequestModel(
                        firstName: firstNameController.text,
                        lastName: lastNameController.text,
                        username: usernameController.text,
                        password: passwordController.text,
                      );
                      context
                          .read<RegisterBloc>()
                          .add(RegisterEvent.register(requestModel));
                    }
                  },
                  margin: const EdgeInsets.all(0),
                  colorButton: dOrangeColor,
                  colorText: Colors.white,
                );
              },
              loading: () => const Center(
                child: CircularProgressIndicator(),
              ),
            );
          },
        );
      }

      return Container(
        // margin: const EdgeInsets.only(top: 60),
        padding: const EdgeInsets.symmetric(
          horizontal: 30,
          vertical: 40,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Column(
          children: [
            Form(
              key: _signUpFormKey,
              child: Column(
                children: [
                  firstNameInput(),
                  const SizedBox(
                    height: 20.0,
                  ),
                  lastNameInput(),
                  const SizedBox(
                    height: 20.0,
                  ),
                  usernameInput(),
                  const SizedBox(
                    height: 20.0,
                  ),
                  passwordInput(),
                  const SizedBox(
                    height: 60.0,
                  ),
                ],
              ),
            ),
            submitButton(),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("DAFTAR"),
        elevation: 0,
        backgroundColor: dOrangeColor,
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          color: dOrangeColor,
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Image.asset(
                "assets/logo.png",
              ),
              const SizedBox(
                height: 50.0,
              ),
              inputSection(),
            ],
          ),
        ),
      ),
    );
  }
}
